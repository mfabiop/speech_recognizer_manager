﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Windows.Phone.Speech.Recognition;

namespace SpeechRecognizerManager
{
    public abstract class SpeechRecognizerState
    {
        /// <summary>
        /// Speech grammar recognized by this state.
        /// </summary>
        public SpeechGrammar Grammar
        {
            get;
            set;
        }

        /// <summary>
        /// Grammar source, it can be Uri or string[]
        /// </summary>
        private object grammarSource;

        public SpeechRecognizerStateManager Manager
        {
            internal set;
            get;
        }

        /// <summary>
        /// The state key.
        /// </summary>
        public string Key
        {
            private set;
            get;
        }

        /// <summary>
        /// Creates a state with given key and grammar
        /// </summary>
        /// <param name="stateKey">State key</param>
        /// <param name="grammarUri">grammarUri</param>
        public SpeechRecognizerState(string stateKey, Uri grammarUri)
        {
            Key = stateKey;
            grammarSource = grammarUri;
        }

        /// <summary>
        /// Creates a state with given key and grammar
        /// </summary>
        /// <param name="stateKey">State key</param>
        /// <param name="phrases">phrases</param>
        public SpeechRecognizerState(string stateKey, IEnumerable<string> phrases)
        {
            Key = stateKey;
            grammarSource = phrases;
        }

        /// <summary>
        /// Initialize the state. It is called only by SpeechRecognizerStateManager instance.
        /// </summary>
        /// <returns>True if the state was initialized, false otherwise.</returns>
        internal bool Initialize()
        {
            try
            {
                if (Manager != null)
                {
                    if (grammarSource is IEnumerable<string>)
                    {
                        Grammar = Manager.AddGrammarFromList(Key, grammarSource as IEnumerable<string>);
                        Grammar.Enabled = false;
                        return true;
                    }
                    else if (grammarSource is Uri)
                    {
                        Grammar = Manager.AddGrammarFromUri(Key, grammarSource as Uri);
                        Grammar.Enabled = false;
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                return false;
            }
            catch (SystemException e)
            {
                MessageBox.Show(e.Message);
                Grammar.Enabled = false;
                return false;
            }
        }

        internal void Terminate()
        {
            Grammar = null;
        }

        /// <summary>
        /// Change the current state of the Manager;
        /// </summary>
        /// <param name="nextStateKey"></param>
        /// <returns>The new current state.</returns>
        public SpeechRecognizerState MoveTo(string nextStateKey)
        {
            Manager.SetCurrentState(nextStateKey);
            return Manager.CurrentState;
        }

        /// <summary>
        /// Process the received speech.
        /// </summary>
        /// <param name="speechWriting"></param>
        public abstract void Process(string speechWriting);

        /// <summary>
        /// Called after enter the state.
        /// </summary>
        public abstract void Enter();

        /// <summary>
        /// Called before exit the state.
        /// </summary>
        public abstract void Exit();

    }
}
