﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Net;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Windows.Foundation;
using Windows.Phone.Speech.Recognition;

namespace SpeechRecognizerManager
{
    /// <summary>
    /// The class that manages the speech recognizer state machine.
    /// </summary>
    public class SpeechRecognizerStateManager
    {
        /// <summary>
        /// Speech recognizer
        /// </summary>
        private SpeechRecognizer recognizer;

        /// <summary>
        /// Background worker for recognizer
        /// </summary>
        private BackgroundWorker recognizerBackgroundWorker;

        /// <summary>
        /// Available states. With the state key.
        /// </summary>
        public Dictionary<string, SpeechRecognizerState> States;

        /// <summary>
        /// Get the current state.
        /// </summary>
        public SpeechRecognizerState CurrentState
        {
            private set;
            get;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public SpeechRecognizerStateManager() 
        {
            recognizer = new SpeechRecognizer();
            recognizerBackgroundWorker = new BackgroundWorker();
            States = new Dictionary<string, SpeechRecognizerState>();
        }

        /// <summary>
        /// Add a grammar from a list. Called by children states.
        /// </summary>
        /// <param name="key">The grammar key.</param>
        /// <param name="phrases">The phrases.</param>
        /// <returns></returns>
        internal SpeechGrammar AddGrammarFromList(string key, IEnumerable<string> phrases)
        {
            return recognizer.Grammars.AddGrammarFromList(key, phrases);
        }

        /// <summary>
        /// Add a grammar from a uri. Called by children states.
        /// </summary>
        /// <param name="key">The grammar key.</param>
        /// <param name="grammarUri">The grammar uri.</param>
        /// <returns></returns>
        internal SpeechGrammar AddGrammarFromUri(string key, Uri grammarUri)
        {
            return recognizer.Grammars.AddGrammarFromUri(key, grammarUri);
        }

        /// <summary>
        /// Initialize the Speech Recognizer manager.
        /// </summary>
        /// <param name="initialState">The initial state after initialized.</param>
        public async void Initialize(string initialState)
        {
            foreach (KeyValuePair<string, SpeechRecognizerState> state in States)
            {
                if (!state.Value.Initialize())
                {
                    Terminate();
                    return;
                }
            }
            SetCurrentState(initialState);

            await recognizer.PreloadGrammarsAsync();                

            recognizerBackgroundWorker.WorkerSupportsCancellation = true;
            recognizerBackgroundWorker.DoWork += new DoWorkEventHandler(speechRecognizerBackgroundWorker_DoWork);
            recognizerBackgroundWorker.RunWorkerAsync();
        }

        /// <summary>
        /// Finalize the Speech Recognizer manager.
        /// </summary>
        public void Terminate()
        {
            foreach (KeyValuePair<string, SpeechRecognizerState> state in States)
            {
                state.Value.Terminate();
            }

            if (recognizer != null)
            {
                recognizer.Dispose();
            }
        }

        /// <summary>
        /// Method that listen to the user, continously. It send the listening text to the right state.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void speechRecognizerBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                while (true)
                {
                    SpeechRecognitionResult result = await recognizer.RecognizeAsync();
                    if (result.TextConfidence == SpeechRecognitionConfidence.High)
                    {
                        CurrentState.Process(result.Text);
                    }
                    Debug.WriteLine(result.Text + " [confidence:" + result.TextConfidence.ToString() + "]");
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// Add a new state.
        /// </summary>
        /// <param name="state">The state to add.</param>
        /// <returns>True if the state has added, false otherwise.</returns>
        public bool AddState(SpeechRecognizerState state)
        {
            if(!States.ContainsKey(state.Key)) 
            {
                state.Manager = this;
                States[state.Key] = state;
                return true;
            }
            return false;
            
        }

        /// <summary>
        /// Remove a state
        /// </summary>
        /// <param name="stateKey">The key of state to be removed</param>
        /// <returns>True if the state has removed, false otherwise</returns>
        public bool RemoveState(string stateKey) 
        {
            return States.Remove(stateKey);
        }

        /// <summary>
        /// Set the current state.
        /// </summary>
        /// <param name="stateKey">The state key.</param>
        /// <returns>True if the new state exist, false otherwise.</returns>
        public bool SetCurrentState(string stateKey)
        {
            if (States.ContainsKey(stateKey))
            {
                if (CurrentState != null)
                {
                    CurrentState.Grammar.Enabled = false;
                    CurrentState.Exit();
                }
                CurrentState = States[stateKey];
                CurrentState.Enter();
                CurrentState.Grammar.Enabled = true;
                return true;
            }
            return false;
        }

    }
}
